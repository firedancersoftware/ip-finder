///-----------------------------------------------------------------
///
/// @file      LookupThread.cpp
/// @author    Firedancer Software
/// Created:   3/05/2012 12:31:07 PM
/// @section   DESCRIPTION
///            LookupThread class implementation
///
///------------------------------------------------------------------

#define _WIN32_WINNT 0x501
#include "LookupThread.h"
#include <winsock2.h>
#include <ws2tcpip.h>

DEFINE_EVENT_TYPE(wxEVT_LOOKUPTHREAD)
LookupThread::LookupThread(wxEvtHandler* pParent, wxString domainName) : wxThread(wxTHREAD_DETACHED), m_pParent(pParent)
{
    //pass parameters into the thread
m_domainName = domainName;
haltThread = false;
}

void* LookupThread::Entry()
{
	wxCommandEvent evt(wxEVT_LOOKUPTHREAD, GetId());

	WSADATA wsaData;
	int iResult;
	INT iRetval;
	DWORD dwRetval;

	int i = 1;
	struct addrinfo *result = NULL;
	struct addrinfo *ptr = NULL;

	struct sockaddr_in  *sockaddr_ipv4;
//    struct sockaddr_in6 *sockaddr_ipv6;
	LPSOCKADDR sockaddr_ip;

	WCHAR ipstringbuffer[46];
	DWORD ipbufferlength = 46;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if(iResult != 0)
	{
		wxString data;
		data << "Failed to initialize WSAStartup - Error: " << iResult;
		evt.SetString(data);
		wxPostEvent(m_pParent, evt);

		evt.SetString(wxT("Thread-End"));
		wxPostEvent(m_pParent, evt);
		return 0;
	}


	dwRetval = getaddrinfo(m_domainName, NULL, NULL, &result);
	if(dwRetval != 0)
	{
		wxString data;
		switch(dwRetval)
		{
			case WSAHOST_NOT_FOUND:
				data << "Host not found";
				break;
			case WSANO_DATA:
				data << "No data found for this domain";
				break;
			case WSATRY_AGAIN:
				data << "Response not received, try again.";
				break;
				default:
				data << "Failed to get address info - Error: " << dwRetval;
				break;
		}
		evt.SetString(data);
		wxPostEvent(m_pParent, evt);
		WSACleanup();

		evt.SetString(wxT("Thread-End"));
		wxPostEvent(m_pParent, evt);

		return 0;
	}

	for(ptr=result; ptr != NULL ;ptr=ptr->ai_next)
	{
//		if(haltThread)
//		{
//			evt.SetString(wxT("Thread-End"));
//			wxPostEvent(m_pParent, evt);
//			return 0;
//		}

		wxString data;

		switch (ptr->ai_family)
		{
			case AF_UNSPEC:
				data << "Unspecified\n";
				break;
			case AF_INET:
				sockaddr_ipv4 = (struct sockaddr_in *) ptr->ai_addr;
				data << inet_ntoa(sockaddr_ipv4->sin_addr) << "\n";
				break;
			case AF_INET6:
//data << "AF_INET6 (IPv6)\n";
				// the InetNtop function is available on Windows Vista and later
				// sockaddr_ipv6 = (struct sockaddr_in6 *) ptr->ai_addr;
				//    InetNtop(AF_INET6, &sockaddr_ipv6->sin6_addr, ipstringbuffer, 46) );

				// We use WSAAddressToString since it is supported on Windows XP and later
				sockaddr_ip = (LPSOCKADDR) ptr->ai_addr;
				// The buffer length is changed by each call to WSAAddresstoString
				// So we need to set it for each iteration through the loop for safety
				ipbufferlength = 46;
				iRetval = WSAAddressToString(sockaddr_ip, (DWORD) ptr->ai_addrlen, NULL,
					ipstringbuffer, &ipbufferlength );
				if (iRetval)
				{
					data << "WSAAddressToString failed with " << WSAGetLastError() << "\n";
				}
				else
				{
					data << ipstringbuffer << "\n";
				}
                break;
 //           case AF_NETBIOS:
//				data << "AF_NETBIOS (NetBIOS)\n";
				break;
			default:
				data << "Other " << ptr->ai_family << "\n";
				break;
		}

		evt.SetString(data);
		wxPostEvent(m_pParent, evt);
	}
	freeaddrinfo(result);
	WSACleanup();

evt.SetString(wxT("Thread-End"));
wxPostEvent(m_pParent, evt);
	return 0;
}

void* LookupThread::CancelLookup()
{
	haltThread = true;
}
