///-----------------------------------------------------------------
///
/// @file      IPFinderFrm.cpp
/// @author    Firedancer Software
/// Created:   3/05/2012 12:31:08 PM
/// @section   DESCRIPTION
///            IPFinderFrm class implementation
///
///------------------------------------------------------------------

#include "IPFinderFrm.h"
#include "LookupThread.h"
#include "CheckForUpdatesThread.h"
#include "Objects/MingW/IPFinder_private.h"
#include <wx/aboutdlg.h>
#include <wx/textfile.h>
#include <wx/treectrl.h>
#include <wx/clipbrd.h>
#include <wx/dataobj.h>
#include <wx/filedlg.h>
#include <wx/dir.h>
#include <wx/file.h>
#include <wx/socket.h>
#include <wx/fileconf.h>
#include <wx/msw/registry.h>
#include <wx/event.h>
#include <wx/accel.h>

#include <wx/msgdlg.h>

//Do not add custom headers between
//Header Include Start and Header Include End
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

//----------------------------------------------------------------------------
// IPFinderFrm
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(IPFinderFrm,wxFrame)
	////Manual Code Start
	EVT_COMMAND(wxID_ANY, wxEVT_LOOKUPTHREAD, IPFinderFrm::OnLookupThread)
	EVT_COMMAND(wxID_ANY, wxEVT_CHECKFORUPDATESTHREAD, IPFinderFrm::OnCheckUpdateThread)
	EVT_TEXT(ID_WXEDIT1, IPFinderFrm::HandleButtonLookupState)
	////Manual Code End111

	EVT_CLOSE(IPFinderFrm::OnClose)
	EVT_MENU(ID_MNU_EXPAND_ALL , IPFinderFrm::MnuexpandallClick)
	EVT_MENU(ID_MNU_COLLAPSEALL , IPFinderFrm::MnucollapseallClick)
	EVT_MENU(ID_MNU_REMOVE , IPFinderFrm::MnuremoveClick)
	EVT_MENU(ID_MNU_EXPAND , IPFinderFrm::MnuexpandClick)
	EVT_MENU(ID_MNU_COLLAPSE , IPFinderFrm::MnucollapseClick)
	EVT_MENU(ID_MNU_LAUNCH , IPFinderFrm::MnulaunchClick)
	EVT_MENU(ID_MNU_COPY , IPFinderFrm::MnucopyClick)
//	EVT_MENU(ID_MNU_EXPAND_ALL_1100 , IPFinderFrm::MnuexpandallClick)
//	EVT_MENU(ID_MNU_COLLAPSEALL_1101 , IPFinderFrm::Mnucollapseal1Click)
//	EVT_MENU(ID_MNU_REMOVE_1102 , IPFinderFrm::Mnuremove1102Click)
	EVT_MENU(wxID_SAVE, IPFinderFrm::MnuSaveClick)
	EVT_MENU(wxID_EXIT, IPFinderFrm::MnuexitClick)
	EVT_MENU(ID_MNU_UPDATE, IPFinderFrm::MnuupdateClick)
	EVT_MENU(ID_MNU_WEBSITE, IPFinderFrm::MnuwebsiteClick)
	EVT_MENU(wxID_ABOUT, IPFinderFrm::MnuaboutClick)
	EVT_BUTTON(ID_WXBUTTONCOPYCLIPBOARD,IPFinderFrm::WxButtonCopyClipboardClick)
	EVT_BUTTON(ID_WXBUTTONCLEARLOG,IPFinderFrm::WxButtonClearLogClick)
	EVT_BUTTON(ID_WXBUTTONSAVELOG,IPFinderFrm::WxButtonSaveLogClick)

//	EVT_KEY_DOWN(IPFinderFrm::KeyboardBinding)

	EVT_TREE_ITEM_MENU(ID_WXTREECTRL1,IPFinderFrm::WxTreeCtrlOnContextMenu)
	EVT_BUTTON(ID_WXBUTTONLOOKUP,IPFinderFrm::WxButtonLookupClick)
	EVT_TEXT_ENTER(ID_WXEDIT1,IPFinderFrm::WxButtonLookupClick)
	END_EVENT_TABLE()
////Event Table End

IPFinderFrm::IPFinderFrm(wxString defaultConfigPath, wxString defaultExportPath, wxString portableAppsPath, wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxFrame(parent, id, title, position, size, style)
{
	SetBackgroundColour(wxString("LIGHTGRAY"));
	configPath = defaultConfigPath;
	exportPath = defaultExportPath;
	pAppsPath = portableAppsPath;

	CreateGUIControls();
    rootItem = WxTreeCtrl1->AddRoot(wxT(""));

	Connect(ID_WXEDIT1, wxEVT_CONTEXT_MENU, wxContextMenuEventHandler(StandardTextCtrl::OnContextMenu), NULL, this);
//	Connect(ID_WXTREECTRL1, wxEVT_CONTEXT_MENU, wxContextMenuEventHandler(IPFinderFrm::WxTreeCtrl1ItemRClick), NULL, this);

//Up to here - need to add detection of installed browsers, common PortableApps browsers and manual browsing to executable
	wxSocketBase::Initialize();

	// Setup shortcut keys for buttons
	wxAcceleratorEntry entries[3];
	entries[0].Set(wxACCEL_CTRL, (int) 'C', ID_WXBUTTONCOPYCLIPBOARD);
	entries[1].Set(wxACCEL_CTRL, (int) 'E', ID_WXBUTTONCLEARLOG);
	entries[2].Set(wxACCEL_CTRL, (int) 'L', ID_WXBUTTONLOOKUP);
	wxAcceleratorTable accel(3, entries);
	this->SetAcceleratorTable(accel);

	bIsLookupRunning = false;
	bIsCheckingUpdates = false;
//	bUpdateSilently = true;
	CheckForUpdates(true);
}

IPFinderFrm::~IPFinderFrm()
{
}

void IPFinderFrm::CreateGUIControls()
{
	////GUI Items Creation Start

	WxBoxSizer1 = new wxBoxSizer(wxVERTICAL);
	this->SetSizer(WxBoxSizer1);
	this->SetAutoLayout(true);

	WxPanel2 = new wxPanel(this, ID_WXPANEL2, wxPoint(10, 10), wxDefaultSize);
	WxBoxSizer1->Add(WxPanel2, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 10);

	WxBoxSizer2 = new wxBoxSizer(wxVERTICAL);
	WxPanel2->SetSizer(WxBoxSizer2);
	WxPanel2->SetAutoLayout(true);

	WxStaticText1 = new wxStaticText(WxPanel2, ID_WXSTATICTEXT1, _("Domain Name:"), wxPoint(136, 5), wxDefaultSize, 0, _("WxStaticText1"));
//	WxStaticText1->SetFont(wxFont(9, wxSWISS, wxNORMAL, wxNORMAL, false, _("Arial")));
	WxBoxSizer2->Add(WxStaticText1, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 5);

	WxPanel3 = new wxPanel(WxPanel2, ID_WXPANEL3, wxPoint(0, 29), wxDefaultSize);
	WxBoxSizer2->Add(WxPanel3, 0, wxALIGN_LEFT | wxEXPAND | wxALL, 0);

	WxBoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel3->SetSizer(WxBoxSizer3);
	WxPanel3->SetAutoLayout(true);

	WxEdit1 = new StandardTextCtrl(WxPanel3, ID_WXEDIT1, _(""), wxPoint(5, 7), wxSize(250, 23), wxTE_PROCESS_ENTER | wxEXPAND, wxDefaultValidator, _("WxEdit1"));
//	WxEdit1->SetFont(wxFont(10, wxSWISS, wxNORMAL, wxNORMAL, false, _("Arial")));
	WxBoxSizer3->Add(WxEdit1, 0, wxALIGN_CENTER | wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL | wxEXPAND | wxALL, 5);
	WxButtonLookup = new wxButton(WxPanel3, ID_WXBUTTONLOOKUP, _("&Lookup"), wxPoint(218, 5), wxDefaultSize, wxEXPAND, wxDefaultValidator, _("WxButtonLookup"));
	WxBoxSizer3->Add(WxButtonLookup, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);
	WxButtonLookup->Disable();


	WxBoxSizer3->AddStretchSpacer(1);

	WxPanel4 = new wxPanel(this, ID_WXPANEL4, wxPoint(10, 99), wxDefaultSize);
	WxBoxSizer1->Add(WxPanel4, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 10);

	WxBoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel4->SetSizer(WxBoxSizer4);
	WxPanel4->SetAutoLayout(true);

	WxTreeCtrl1 = new wxTreeCtrl(WxPanel4, ID_WXTREECTRL1, wxPoint(5, 5), wxSize(346, 115), wxTR_HAS_BUTTONS | wxTR_LINES_AT_ROOT | wxTR_HIDE_ROOT, wxDefaultValidator, _("WxTreeCtrl1"));
	WxBoxSizer4->Add(WxTreeCtrl1, 1, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxPanel5 = new wxPanel(this, ID_WXPANEL5, wxPoint(10, 247), wxDefaultSize);
	WxBoxSizer1->Add(WxPanel5, 0, wxALIGN_RIGHT | wxALL, 10);

	WxBoxSizer5 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel5->SetSizer(WxBoxSizer5);
	WxPanel5->SetAutoLayout(true);

	WxButtonSaveLog = new wxButton(WxPanel5, ID_WXBUTTONSAVELOG, _("&Save Log"), wxPoint(5, 5), wxDefaultSize, wxEXPAND, wxDefaultValidator, _("WxButtonSaveLog"));
	WxButtonSaveLog->Disable();
	WxBoxSizer5->Add(WxButtonSaveLog, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxButtonClearLog = new wxButton(WxPanel5, ID_WXBUTTONCLEARLOG, _("&Empty Log"), wxPoint(126, 5), wxDefaultSize, wxEXPAND, wxDefaultValidator, _("WxButtonClearLog"));
	WxButtonClearLog->Disable();
	WxBoxSizer5->Add(WxButtonClearLog, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxButtonCopyClipboard = new wxButton(WxPanel5, ID_WXBUTTONCOPYCLIPBOARD, _("&Copy to Clipboard"), wxPoint(247, 5), wxDefaultSize, wxEXPAND, wxDefaultValidator, _("WxButtonCopyClipboard"));
	WxButtonCopyClipboard->Disable();
	WxBoxSizer5->Add(WxButtonCopyClipboard, 0, wxALIGN_CENTER | wxEXPAND | wxALL, 5);

	WxMenuBar1 = new wxMenuBar();
	wxMenu *ID_MNU_FILE_1001_Mnu_Obj = new wxMenu();
	ID_MNU_FILE_1001_Mnu_Obj->Append(wxID_SAVE, "&Save", _(""), wxITEM_NORMAL);
	ID_MNU_FILE_1001_Mnu_Obj->Enable(wxID_SAVE,false);
	ID_MNU_FILE_1001_Mnu_Obj->AppendSeparator();
	ID_MNU_FILE_1001_Mnu_Obj->Append(wxID_EXIT, _("E&xit"), _(""), wxITEM_NORMAL);
	WxMenuBar1->Append(ID_MNU_FILE_1001_Mnu_Obj, _("&File"));

	wxMenu *ID_MNU_HELP_1003_Mnu_Obj = new wxMenu();
	ID_MNU_HELP_1003_Mnu_Obj->Append(ID_MNU_UPDATE, _("&Check for Updates"), _(""), wxITEM_NORMAL);
	ID_MNU_HELP_1003_Mnu_Obj->AppendSeparator();
	ID_MNU_HELP_1003_Mnu_Obj->Append(ID_MNU_WEBSITE, _("&Website"), _(""), wxITEM_NORMAL);
	ID_MNU_HELP_1003_Mnu_Obj->Append(wxID_ABOUT, _("&About"), _(""), wxITEM_NORMAL);
	WxMenuBar1->Append(ID_MNU_HELP_1003_Mnu_Obj, _("&Help"));
	SetMenuBar(WxMenuBar1);

	WxOpenWithSubMenu = new wxMenu("");
//	WxOpenWithSubMenu->Append(ID_MNU_LAUNCH, _("Default Browser"), _(""), wxITEM_NORMAL);

	systemBrowserPaths.Empty();

	wxRegKey installedBrowsersKey(wxRegKey::HKLM, "Software\\Clients\\StartMenuInternet");

	if(installedBrowsersKey.Exists())
	{
		if(installedBrowsersKey.Open(wxRegKey::Read))
		{
			size_t subkeys;
			installedBrowsersKey.GetKeyInfo(&subkeys, NULL, NULL, NULL);

			wxString keyName;
			long index;
			installedBrowsersKey.GetFirstKey(keyName, index);

			for(int i = 0; i < subkeys; i++)
			{
				wxRegKey wxBrowserKey(installedBrowsersKey, keyName);
				wxString browserName = wxBrowserKey.QueryDefaultValue();

				wxRegKey wxCommandKey(wxBrowserKey, "shell\\open\\command");
				if(wxCommandKey.Exists())
				{
					wxString browserPath = wxCommandKey.QueryDefaultValue();
					systemBrowserPaths.Add(browserPath);

					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, browserName, _(""), wxITEM_NORMAL);


//					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId, (wxObject *)&browserPath);
					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
//					  this->Connect(newId, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(IPFinderFrm::OpenInBrowser));

				}

			    installedBrowsersKey.GetNextKey(keyName, index);
			}
		}
	}

	if(!pAppsPath.IsEmpty())
	{
		// Already know that pAppsPath is a valid directory if it isn't empty
		wxDir dir(pAppsPath);
		if(dir.IsOpened())
		{
			wxString dirName = dir.GetName();

			wxString fileName;
			bool cont = dir.GetFirst(&fileName, wxEmptyString, wxDIR_DIRS | wxDIR_HIDDEN);

			while(cont)
			{
				// Only enumerate directories ending in "Portable"
				if(fileName.IsSameAs("GoogleChromePortable"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Google Chrome Portable", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\GoogleChromePortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("GoogleChromePortableBeta"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Google Chrome Portable (Beta)", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\GoogleChromePortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("GoogleChromePortableDev"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Google Chrome Portable (Dev)", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\GoogleChromePortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("LinksPortable"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Links Portable", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\LinksPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("LynxPortable"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Lynx Portable", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\LynxPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("MaxthonPortable"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Maxthon Portable", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\MaxthonPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("FirefoxPortable"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Mozilla Firefox, Portable Edition", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\FirefoxPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("FirefoxPortableESR"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Mozilla Firefox ESR, Portable Edition", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\FirefoxPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("FirefoxPortableAurora"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Mozilla Firefox, Portable Edition (Aurora)", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\FirefoxPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("FirefoxPortableTest"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Mozilla Firefox, Portable Edition (Beta)", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\FirefoxPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("FirefoxPortableNightly"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Mozilla Firefox, Portable Edition (Nightly)", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\FirefoxPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("FirefoxPortableNightly64"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Mozilla Firefox, Portable Edition (Nightly 64-bit)", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\FirefoxPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("IronPortable"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Iron Portable", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\IronPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("OperaPortable"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Opera, Portable Edition", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\OperaPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("QupZillaPortable"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "QupZilla Portable", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\QupZillaPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				else if(fileName.IsSameAs("SeaMonkeyPortable"))
				{
					wxWindowID newId = wxWindow::NewControlId();
					WxOpenWithSubMenu->Append(newId, "Seamonkey, Portable Edition", _(""), wxITEM_NORMAL);

					wxString browserPath = dirName + "\\" + fileName + "\\SeaMonkeyPortable.exe";
					systemBrowserPaths.Add(browserPath);

					Bind(wxEVT_COMMAND_MENU_SELECTED, &IPFinderFrm::OpenInBrowser, this, newId, newId);
				}
				cont = dir.GetNext(&fileName);
			}
		}
	}

	WxTreeCtrlPopupMenu = new wxMenu(_(""));
	WxTreeCtrlPopupMenu->Append(ID_MNU_LAUNCH, _("Open in Default Browser"), _(""), wxITEM_NORMAL);
	WxTreeCtrlPopupMenu->AppendSubMenu(WxOpenWithSubMenu, _("Open With..."));
	WxTreeCtrlPopupMenu->Append(ID_MNU_COPY, _("Copy To Clipboard"), _(""), wxITEM_NORMAL);
	WxTreeCtrlPopupMenu->Append(ID_MNU_REMOVE, _("Remove From Results"), _(""), wxITEM_NORMAL);
	WxTreeCtrlPopupMenu->AppendSeparator();
	WxTreeCtrlPopupMenu->Append(ID_MNU_EXPAND, _("Expand"), _(""), wxITEM_NORMAL);
	WxTreeCtrlPopupMenu->Append(ID_MNU_COLLAPSE, _("Collapse"), _(""), wxITEM_NORMAL);
	WxTreeCtrlPopupMenu->Append(ID_MNU_EXPAND_ALL, _("Expand All"), _(""), wxITEM_NORMAL);
	WxTreeCtrlPopupMenu->Append(ID_MNU_COLLAPSEALL, _("Collapse All"), _(""), wxITEM_NORMAL);


	SetTitle(_("IPFinder"));
	SetIcon(wxNullIcon);

	Center();
	WxBoxSizer1->SetSizeHints(this);
	this->Layout();
	////GUI Items Creation End
}


void IPFinderFrm::OpenInBrowser(wxCommandEvent& event)
{
	int menuItemId = event.GetId();
	size_t position;
	wxMenuItem *menuItem = WxOpenWithSubMenu->FindChildItem(menuItemId, &position);

	if(position != wxNOT_FOUND)
	{
		wxString execString = systemBrowserPaths.Item(position);

		wxTreeItemId item = WxTreeCtrl1->GetSelection();
		if(item.IsOk())
		{
			execString << " " << WxTreeCtrl1->GetItemText(item);
			wxExecute(execString);
		}
	}
}


void IPFinderFrm::OnClose(wxCloseEvent& event)
{
	Destroy();
}

/*
 * MnuexitClick
 */
void IPFinderFrm::MnuexitClick(wxCommandEvent& event)
{
	Close(true);
}

/*
 * MnuwebsiteClick
 */
void IPFinderFrm::MnuwebsiteClick(wxCommandEvent& event)
{
	wxLaunchDefaultBrowser(wxT("http://firedancer-software.com/software/ipfinder"));
}

/*
 * MnuaboutClick
 */
void IPFinderFrm::MnuaboutClick(wxCommandEvent& event)
{
	wxAboutDialogInfo info;
	info.SetName(PRODUCT_NAME);
    info.SetVersion(PRODUCT_VERSION);
    info.SetCopyright(_("(C) 2012-2014 Firedancer Software.\nLicenced under the GPL Version 3.\n"));
    info.SetDescription(_("Resolve domain names to IP addresses."));

	wxAboutBox(info);
}

/*
 * MnuupdateClick
 */
void IPFinderFrm::MnuupdateClick(wxCommandEvent& event)
{
	CheckForUpdates(false);
}



/*
 * WxButtonLookupClick
 */
void IPFinderFrm::WxButtonLookupClick(wxCommandEvent& event)
{
	if(WxButtonLookup->IsEnabled())
	{
		wxString msg;
		msg = WxEdit1->GetLineText(0);
		if(msg == "")
		{
			return;
		}
		WxButtonLookup->Disable();
		DomainLookup(msg);
	}
}

/*
 * WxButtonClearClick
 */
void IPFinderFrm::WxButtonClearClick(wxCommandEvent& event)
{
	WxEdit1->SetFocus();

}

void IPFinderFrm::DomainLookup(wxString domainName)
{
	bIsLookupRunning = true;
	currentChild = WxTreeCtrl1->AppendItem(rootItem, domainName);

    LookupThread *thread = new LookupThread(this, domainName);
    thread->Create();
    thread->Run();
}


void IPFinderFrm::CheckForUpdates(bool bCheckSilently)
{
	bIsCheckingUpdates = true;
//	bUpdateSilently = bCheckSilently;
	WxMenuBar1->Enable(ID_MNU_UPDATE, false);
    CheckForUpdatesThread *thread = new CheckForUpdatesThread(this, bCheckSilently);
    thread->Create();
    thread->Run();
}

void IPFinderFrm::ResetOutput()
{
	WxTreeCtrl1->DeleteChildren(rootItem);

    WxMenuBar1->Enable(wxID_SAVE, false);
	WxButtonSaveLog->Disable();
	WxButtonClearLog->Disable();
	WxButtonCopyClipboard->Disable();
}

void IPFinderFrm::OnLookupThread(wxCommandEvent& event)
{
	wxString temp = event.GetString();
	if(temp == "Thread-End")
	{
		bIsLookupRunning = false;
		if(WxEdit1->GetLineLength(0) > 0)
		{
			WxButtonLookup->Enable();
		}
		else
		{
			WxButtonLookup->Disable();
		}
	    WxMenuBar1->Enable(wxID_SAVE, true);
	    WxButtonSaveLog->Enable();
	    WxButtonClearLog->Enable();
	    WxButtonCopyClipboard->Enable();
		return;
	}
	OutputMessage(temp.Trim());
}

void IPFinderFrm::OnCheckUpdateThread(wxCommandEvent& event)
{
	WxMenuBar1->Enable(ID_MNU_UPDATE, true);
	bIsCheckingUpdates = false;
	return;
}


void IPFinderFrm::OutputMessage(wxString message)
{
    WxTreeCtrl1->AppendItem(currentChild, message);

    if(!WxTreeCtrl1->IsExpanded(currentChild))
    {
        WxTreeCtrl1->Expand(currentChild);
    }

     WxTreeCtrl1->SortChildren(currentChild);
}


/*
 * WxButtonSaveLogClick
 */
void IPFinderFrm::WxButtonSaveLogClick(wxCommandEvent& event)
{
	SaveLogToFile();
}

void IPFinderFrm::SaveLogToFile()
{
	wxFileDialog saveFileDialog(this, _("Save log file"), exportPath, "IPFinderLog.txt", "*.txt", wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
	if(saveFileDialog.ShowModal() == wxID_CANCEL)
	{
		return;
	}
	wxFile *output = new wxFile(saveFileDialog.GetPath(), wxFile::write);

	if(output->IsOpened())
	{
		output->Write(GetFormattedResults());
		output->Close();
	}
}

/*
 * WxButtonClearLogClick
 */
void IPFinderFrm::WxButtonClearLogClick(wxCommandEvent& event)
{
    ResetOutput();
}


/*
 * WxButtonCopyClipboardClick
 */
void IPFinderFrm::WxButtonCopyClipboardClick(wxCommandEvent& event)
{
	CopyToClipboard(GetFormattedResults());
}


wxString IPFinderFrm::GetFormattedResults()
{
	wxString output = "";
	wxTreeItemIdValue cookie;
	wxTreeItemId child = WxTreeCtrl1->GetFirstChild(rootItem, cookie);

	while(child.IsOk())
	{
		output << wxT("[") << WxTreeCtrl1->GetItemText(child) << wxT("]") << wxTextFile::GetEOL();
		if(WxTreeCtrl1->ItemHasChildren(child))
		{
			wxTreeItemIdValue innerCookie;
			wxTreeItemId innerChild = WxTreeCtrl1->GetFirstChild(child, innerCookie);

			while(innerChild.IsOk())
			{
				output << WxTreeCtrl1->GetItemText(innerChild);
				innerChild = WxTreeCtrl1->GetNextChild(child, innerCookie) ;
				output << wxTextFile::GetEOL();
			}

			output << wxTextFile::GetEOL();
		}
		child = WxTreeCtrl1->GetNextChild(rootItem, cookie) ;
	}

	return output;
}

/*
 * MnuSaveClick
 */
void IPFinderFrm::MnuSaveClick(wxCommandEvent& event)
{
	SaveLogToFile();
}

/*
 * WxTreeCtrlOnContextMenu
 */
void IPFinderFrm::WxTreeCtrlOnContextMenu(wxTreeEvent& event)
{
	WxTreeCtrl1->SelectItem(event.GetItem());

	wxTreeItemId selectedItem = WxTreeCtrl1->GetSelection();
	if(selectedItem.IsOk())
	{
		wxPoint position = wxDefaultPosition;

		wxRect itemRect;
		WxTreeCtrl1->GetBoundingRect(selectedItem, itemRect, true);

		wxRect ctrlRect = WxTreeCtrl1->GetScreenRect();
		wxRect actualRect(ctrlRect.GetX() + itemRect.GetX(), ctrlRect.GetY() + itemRect.GetY(), itemRect.GetWidth(), itemRect.GetHeight());
		if(!actualRect.Contains(wxGetMousePosition()))
		{
			position = ScreenToClient(actualRect.GetBottomLeft());
		}

		// First level items only
		if(WxTreeCtrl1->GetItemParent(selectedItem) == WxTreeCtrl1->GetRootItem())
		{
			if(!WxTreeCtrl1->ItemHasChildren(selectedItem))
			{
				WxTreeCtrlPopupMenu->Enable(ID_MNU_EXPAND, false);
				WxTreeCtrlPopupMenu->Enable(ID_MNU_COLLAPSE, false);
			}
			else
			{
				WxTreeCtrlPopupMenu->Enable(ID_MNU_EXPAND, true);
				WxTreeCtrlPopupMenu->Enable(ID_MNU_COLLAPSE, true);
			}
				WxTreeCtrlPopupMenu->Enable(ID_MNU_LAUNCH, true);

				WxTreeCtrlPopupMenu->SetLabel(ID_MNU_EXPAND, wxT("Expand"));
				WxTreeCtrlPopupMenu->SetLabel(ID_MNU_COLLAPSE, wxT("Collapse"));
		}
		else
		{
			WxTreeCtrlPopupMenu->SetLabel(ID_MNU_EXPAND, wxT("Expand Parent"));
			WxTreeCtrlPopupMenu->SetLabel(ID_MNU_COLLAPSE, wxT("Collapse Parent"));
			WxTreeCtrlPopupMenu->Enable(ID_MNU_EXPAND, true);
			WxTreeCtrlPopupMenu->Enable(ID_MNU_COLLAPSE, true);

			wxString itemText = WxTreeCtrl1->GetItemText(selectedItem);
			if(itemText.StartsWith("Failed to initialize WSAStartup")
			|| itemText == "Host not found"
			|| itemText == "No data found for this domain"
			|| itemText == "Response not received, try again"
			|| itemText.StartsWith("Failed to get address info")
			|| itemText == "Unspecified"
			|| itemText.StartsWith("WSAAddressToString failed")
			|| itemText.StartsWith("Other"))
			{
				WxTreeCtrlPopupMenu->Enable(ID_MNU_LAUNCH, false);
			}
			else
			{
				WxTreeCtrlPopupMenu->Enable(ID_MNU_LAUNCH, true);
			}
		}
		wxWindow::PopupMenu(WxTreeCtrlPopupMenu, position);
	}
}

/*
 * MnuexpandallClick
 */
void IPFinderFrm::MnuexpandallClick(wxCommandEvent& event)
{
	WxTreeCtrl1->ExpandAll();
}

/*
 * MnucollapseallClick
 */
void IPFinderFrm::MnucollapseallClick(wxCommandEvent& event)
{
	WxTreeCtrl1->CollapseAll();
}

/*
 * MnuexpandClick
 */
void IPFinderFrm::MnuexpandClick(wxCommandEvent& event)
{
	wxTreeItemId item = WxTreeCtrl1->GetSelection();

	if(item.IsOk())
	{
		if(!WxTreeCtrl1->ItemHasChildren(item))
		{
			item = WxTreeCtrl1->GetItemParent(item);
		}
		WxTreeCtrl1->Expand(item);
	}
}

/*
 * MnucollapseClick
 */
void IPFinderFrm::MnucollapseClick(wxCommandEvent& event)
{
	wxTreeItemId item = WxTreeCtrl1->GetSelection();
	if(item.IsOk())
	{
		if(!WxTreeCtrl1->ItemHasChildren(item))
		{
			item = WxTreeCtrl1->GetItemParent(item);
		}
		WxTreeCtrl1->Collapse(item);
	}
}

/*
 * MnuremoveClick
 */
void IPFinderFrm::MnuremoveClick(wxCommandEvent& event)
{
	wxTreeItemId item = WxTreeCtrl1->GetSelection();
	if(item.IsOk())
	{
		WxTreeCtrl1->Delete(item);

		if(!WxTreeCtrl1->ItemHasChildren(WxTreeCtrl1->GetRootItem()))
		{
			WxButtonSaveLog->Disable();
			WxButtonClearLog->Disable();
			WxButtonCopyClipboard->Disable();
		}
	}
}

/*
 * MnulaunchClick
 */
void IPFinderFrm::MnulaunchClick(wxCommandEvent& event)
{
	wxTreeItemId item = WxTreeCtrl1->GetSelection();
	if(item.IsOk())
	{
		wxLaunchDefaultBrowser(WxTreeCtrl1->GetItemText(item));
//		WxTreeCtrl1->Delete(item);
	}
}

/*
 * MnucopyClick
 */
void IPFinderFrm::MnucopyClick(wxCommandEvent& event)
{
	wxTreeItemId item = WxTreeCtrl1->GetSelection();
	if(item.IsOk())
	{
		CopyToClipboard(WxTreeCtrl1->GetItemText(item));
	}
}

/*
 * WxLookupValidate
 */
void IPFinderFrm::HandleButtonLookupState(wxCommandEvent& event)
{
	if(WxEdit1->GetLineLength(0) > 0 && !bIsLookupRunning)
	{
		WxButtonLookup->Enable();
	}
	else
	{
		WxButtonLookup->Disable();
	}
}


void IPFinderFrm::CopyToClipboard(wxString text)
{
	if (wxTheClipboard->Open())
	{
		wxTheClipboard->SetData(new wxTextDataObject(text));
		wxTheClipboard->Close();
	}
}
/*
void IPFinderFrm::KeyboardBinding(wxKeyEvent& event)
{
	wxMessageBox("Going", "Good");
	if(event.GetModifiers() == wxMOD_CONTROL)
	{
		switch(event.GetKeyCode())
		{
		    case 'C': // Handle "Copy To Clipboard" shortcut
				CopyToClipboard(GetFormattedResults());
		        break;
		    case 'E': // Handle "Empty Results" shortcut
    			ResetOutput();
		        break;
		    case 'L': // Handle "Lookup" shortcut
				if(WxButtonLookup->IsEnabled())
				{
					wxString msg;
					msg = WxEdit1->GetLineText(0);
					if(msg == "")
					{
						return;
					}
					WxButtonLookup->Disable();
					DomainLookup(msg);
				}
		        break;
    		default: // nothing
        		break;
		}
	}
}
*/
