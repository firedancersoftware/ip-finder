///-----------------------------------------------------------------
///
/// @file      CheckForUpdatesThread.cpp
/// @author    Firedancer Software
/// Created:   3/05/2012 12:31:08 PM
/// @section   DESCRIPTION
///            CheckForUpdatesThread class declaration
///
///------------------------------------------------------------------

#ifndef CHECKFORUPDATESTHREAD_H
#define CHECKFORUPDATESTHREAD_H

#include <wx/thread.h>
#include <wx/event.h>

BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_EVENT_TYPE(wxEVT_CHECKFORUPDATESTHREAD, -1)
END_DECLARE_EVENT_TYPES()


class CheckForUpdatesThread : public wxThread
{
    public:
        CheckForUpdatesThread(wxEvtHandler* pParent, bool bCheckSilently);
    private:
        void* Entry();
    protected:
		bool b_CheckSilently;
        wxEvtHandler* m_pParent;
};
#endif
