///-----------------------------------------------------------------
///
/// @file      StandardTextCtrl.cpp
/// @author    Firedancer Software
/// Created:   3/05/2012 12:31:07 PM
/// @section   DESCRIPTION
///            StandardTextCtrl class implementation
///
///------------------------------------------------------------------

#include "StandardTextCtrl.h"
#include <wx/menu.h>

BEGIN_EVENT_TABLE(StandardTextCtrl, wxTextCtrl)
    EVT_CONTEXT_MENU(StandardTextCtrl::OnContextMenu)
END_EVENT_TABLE()

void StandardTextCtrl::OnContextMenu(wxContextMenuEvent& event)
{
	wxPoint position = wxDefaultPosition;
	wxTextCtrl *WxTextControl = wxDynamicCast(event.GetEventObject(), wxTextCtrl);

	WxTextControl->SetFocus();
	if(event.GetPosition() == wxDefaultPosition)
	{
		wxRect rect = WxTextControl->GetScreenRect();
		position = ScreenToClient(wxPoint(rect.GetBottomLeft().x, rect.GetBottomLeft().y - 4));
	}

	wxMenu* menu = new wxMenu;
	menu->Append(wxID_UNDO, _("&Undo"));
	menu->Append(wxID_REDO, _("&Redo"));
	menu->AppendSeparator();
	menu->Append(wxID_CUT, _("Cu&t"));
	menu->Append(wxID_COPY, _("&Copy"));
	menu->Append(wxID_PASTE, _("&Paste"));
	menu->Append(wxID_CLEAR, _("&Delete"));
	menu->AppendSeparator();
	menu->Append(wxID_SELECTALL, _("Select &All"));

	PopupMenu(menu, position);
}
