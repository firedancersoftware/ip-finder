///-----------------------------------------------------------------
///
/// @file      IPFinderApp.cpp
/// @author    Firedancer Software
/// Created:   3/05/2012 12:31:08 PM
/// @section   DESCRIPTION
///            IPFinderApp class implementation
///
///------------------------------------------------------------------

#include "IPFinderApp.h"
#include "IPFinderFrm.h"
#include <wx/stdpaths.h>
#include <wx/filefn.h>
#include <wx/protocol/http.h>

IMPLEMENT_APP(IPFinderFrmApp)

bool IPFinderFrmApp::OnInit()
{
	parser.SetDesc(g_cmdLineDesc);
	parser.SetCmdLine(argc, argv);
	parser.SetSwitchChars(wxT("-"));
	int iParseVal = parser.Parse();

	if(iParseVal != 0)
	{
		return 0;
	}

	wxHTTP::Initialize();

	wxString appPath = wxStandardPaths::Get().GetExecutablePath();
	defaultExportPath = wxPathOnly(appPath);
	parser.Found(wxT("e"), &defaultExportPath);
//	defaultConfigPath = wxPathOnly(appPath);
//	parser.Found(wxT("c"), &defaultConfigPath);

	wxString pAppsPath;
	if(parser.Found(wxT("portableappspath"), &pAppsPath) && wxDirExists(pAppsPath))
	{
		portableAppsPath = pAppsPath;
	}


	IPFinderFrm* frame = new IPFinderFrm(defaultConfigPath, defaultExportPath, portableAppsPath);
	SetTopWindow(frame);

	frame->SetIcon(wxICON(appicon));
	frame->Show();
	return true;
}

int IPFinderFrmApp::OnExit()
{
	return 0;
}
